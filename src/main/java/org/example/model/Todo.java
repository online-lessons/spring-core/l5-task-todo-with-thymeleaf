package org.example.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Todo {

    private static int c = 1;

    Integer id;
    String title;
    String priority;
    LocalDateTime createdAt;

    public Todo(String title, String priority) {
        id = c++;
        this.title = title;
        this.priority = priority;
        createdAt = LocalDateTime.now();
    }
}
