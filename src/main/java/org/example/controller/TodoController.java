
package org.example.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.example.model.Todo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TodoController {
    private final List<Todo> todos = new ArrayList<>();

    @GetMapping("/todo")
    public String todoPage(Model model) {
        model.addAttribute("todos", todos);
        return "todo";
    }

    @GetMapping("/todo/add")
    public String todoAddPage() {
        return "todo-add";
    }

    @PostMapping("/todo/add")
    public String todoAdd(HttpServletRequest req) {
        String title = req.getParameter("title");
        String priority = req.getParameter("priority");

        todos.add(new Todo(title, priority));

        return "redirect:/todo";
    }

    @GetMapping("/todo/edit")
    public String todoEditPage(Model model, HttpServletRequest req) {
        Integer id = Integer.valueOf(req.getParameter("id"));
        Todo todo = todos.stream().filter(t -> t.getId().equals(id)).findFirst().orElse(null);
        model.addAttribute("todo", todo);
        return "todo-edit";
    }

    @PostMapping("/todo/edit")
    public String todoEdit(HttpServletRequest req) {
        Integer id = Integer.valueOf(req.getParameter("id"));
        String title = req.getParameter("title");
        String priority = req.getParameter("priority");
        Todo todo = todos.stream().filter(t -> t.getId().equals(id)).findFirst().orElse(null);
        if (todo != null) {
            todo.setTitle(title);
            todo.setPriority(priority);
        }
        return "redirect:/todo";
    }

    @GetMapping("/todo/delete")
    public String deletePage(HttpServletRequest req, Model model) {
        Integer id = Integer.valueOf(req.getParameter("id"));
        model.addAttribute("todo", todos.stream().filter(t -> t.getId().equals(id)).findFirst().orElse(null));
        return "todo-delete";
    }

    @PostMapping("/todo/delete")
    public String delete(HttpServletRequest req) {
        Integer id = Integer.valueOf(req.getParameter("id"));
        for (Todo todo : todos) {
            if (todo.getId().equals(id)) {
                todos.remove(todo);
                break;
            }
        }
        return "redirect:/todo";
    }

}
